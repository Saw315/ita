const express = require('express');
const router = express.Router();
const RecordService = require('../service/record.service');

router.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
});

router.get('/', (req, res) => {
    RecordService.getRecord();
});

module.exports = router;